(async () => {
    const cron = require('node-cron');
    const axios = require('axios').default;
    const serverUrl = process.env.LIGHT_CONTROLLER_SERVER
    const sunriseSunsetJs = require("sunrise-sunset-js")
    
    let task
    let updateSchedule = () => {
        console.log(`Update schedule at ${new Date(Date.now())}`);
        let sunsetTime = sunriseSunsetJs.getSunset(43.787140, -70.410599)
        console.log(`Updating schedule to turn lights on at ${sunsetTime}`)
        task?.destroy()
        task = cron.schedule(`${sunsetTime.getMinutes()} ${sunsetTime.getHours()} * * *`, () => {
            console.log(`Turn on lights at ${new Date(Date.now())}`);
            axios.put(`${serverUrl}/setPattern?value=1`)
        });
    }

    updateSchedule()
    cron.schedule('0 0 * * *', () => {
        updateSchedule()
    })

    cron.schedule('0 0 * * *', () => {
        console.log(`Turn off lights at ${new Date(Date.now())}`);
        axios.put(`${serverUrl}/setPattern?value=0`)
    });
})()