#!/usr/bin/bash

echo "America/New_York" > /etc/timezone
dpkg-reconfigure tzdata

ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime

node schedule.js