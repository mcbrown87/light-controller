import RPi.GPIO as GPIO

class Lights:

    GPIO.setmode(GPIO.BCM)
    
    def __init__(self):
        self.pins = [21, 20, 26, 19, 13, 22, 27, 17]
        for pin in self.pins:
            GPIO.setup(pin, GPIO.OUT)

    def set_state(self, bitmap):
        for i in range(8):
            if bitmap & (1 << i):
                GPIO.output(self.pins[i], GPIO.HIGH)
            else:
                GPIO.output(self.pins[i], GPIO.LOW)      