import time 
import configparser
import os
import asyncio
import websockets
try:
    from Lights import Lights
except (RuntimeError, ModuleNotFoundError):
    from MockLights import MockLights

class LightController:
    def __init__(self, useMockGPIO=False):
        
        if useMockGPIO:
            self.lights = MockLights()
        else:
            self.lights = Lights()
        
        self.config = configparser.ConfigParser()

        self.websocketServerEndpoint = os.getenv('WEBSOCKET_ENDPOINT')
        if self.websocketServerEndpoint is None:
            self.websocketServerEndpoint = "ws://localhost:80/"

        self.refreshRate = .05
        self.patternSelection = 2
        self.patterns = [[
            0b00000000
        ], [
            0b11111111
        ], [
            0b10000000,
            0b01000000,
            0b00100000,
            0b00010000,
            0b00001000,
            0b00000100,
            0b00000010,
            0b00000001,
            0b00000010,
            0b00000100,
            0b00001000,
            0b00010000,
            0b00100000,
            0b01000000
        ], [
            0b00000000,
            0b11111111
        ], [
            0b10000001,
            0b01000010,
            0b00100100,
            0b00011000,
            0b00100100,
            0b01000010
        ], [
            0b11000000,
            0b00110000,
            0b00001100,
            0b00000011
        ], [
            0b00000001,
            0b00000011,
            0b00000111,
            0b00001111,
            0b00011111,
            0b00111111,
            0b01111111,
            0b11111111,
            0b11111110,
            0b11111100,
            0b11111000,
            0b11110000,
            0b11100000,
            0b11000000,
            0b10000000,
            0b00000000,
            0b00000000,
            0b00000000,
            0b00000000,
            0b00000000
        ], [
            0b10000000,
            0b01000000,
            0b00100000,
            0b00010000,
            0b00001000,
            0b00000100,
            0b00000010,
            0b00000001,
            0b10000001,
            0b01000001,
            0b00100001,
            0b00010001,
            0b00001001,
            0b00000101,
            0b00000011,
            0b10000011,
            0b01000011,
            0b00100011,
            0b00010011,
            0b00001011,
            0b00000111,
            0b10000111,
            0b01000111,
            0b00100111,
            0b00010111,
            0b00001111,
            0b10001111,
            0b01001111,
            0b00101111,
            0b00011111,
            0b10011111,
            0b01011111,
            0b00111111,
            0b10111111,
            0b01111111,
            0b11111111,
            0b11111111,
            0b11111111,
            0b11111111,
            0b11111111,
            0b11111111,
            0b11111110,
            0b11111100,
            0b11111000,
            0b11110000,
            0b11100000,
            0b11000000,
            0b10000000,
            0b00000000,
            0b00000000,
            0b00000000,
            0b00000000,
            0b00000000,
            0b00000000,
        ], [
            0b00000111,
            0b00001011,
            0b00010011,
            0b00100011,
            0b01000011,
            0b10000011
        ]]

    async def start_rendering(self):
        async with websockets.connect(self.websocketServerEndpoint) as websocket:
            count = 0
            while True:
                pattern = self.patterns[self.patternSelection]
                lightState = pattern[count % len(pattern)]
                self.lights.set_state(lightState)
                await websocket.send(str(lightState))
                time.sleep(self.refreshRate)
                count += 1
                self.update_config()

    def update_config(self):
        try:
            self.config.read('./sharedConfig/lightcontroller.ini')
            lightControllerSection = self.config['LightController']
            self.refreshRate = float(lightControllerSection['RefreshRate'])
            self.patternSelection = int(lightControllerSection['PatternSelection'])
        except:
            pass

asyncio.get_event_loop().run_until_complete(LightController().start_rendering())