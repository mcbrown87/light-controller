import './App.css';
import React from 'react';
import Slider from '@material-ui/core/Slider';
import Grid from '@material-ui/core/Grid';
import Websocket from 'react-websocket';
import Thermometer from 'react-thermometer-component';

export default class App extends React.Component {
  constructor() {
    super()

    this.state = {
      currentRefreshRate: "",
      currentPattern: 0,
      lightPattern: 0,
      temperature: null
    }

    this.onRefreshRateSelect = this.onRefreshRateSelect.bind(this)
    this.onPatternSelect = this.onPatternSelect.bind(this)
    this.handleData = this.handleData.bind(this)

    fetch(`getRefreshRate`)
      .then(res => res.json()) 
      .then(res => {
        this.setState(() => ({
          currentRefreshRate: res.refreshRate
        })) 
      }) 

    fetch(`getTemperature`)
      .then(res => res.json()) 
        .then(res => {
          this.setState(() => ({
            temperature: res.temperature
          })) 
        }) 

    fetch(`getPattern`)
      .then(res => res.json()) 
      .then(res => {
        this.setState(() => ({
          currentPattern: res.pattern
        })) 
      }) 
    }

  handleData(data) {
    this.setState(() => ({
      lightPattern: (parseInt(data) >>> 0).toString(2).padStart(8, "0")
    }))
  }

  onPatternSelect(e, newValue) {
    this.setState(() => ({
      currentPattern: newValue
    }))

    fetch(`setPattern?value=${newValue}`, {
      method: 'put'
    })
  }

  onRefreshRateSelect(e, newValue) {
    this.setState(() => ({
      currentRefreshRate: newValue
    }))

    fetch(`setRefreshRate?value=${newValue}`, {
      method: 'put'
    })
  }

  render() {
    return (
      <div className="App">
        <Slider
              defaultValue={.05}
              aria-labelledby="discrete-slider"
              valueLabelDisplay="auto"
              value={this.state.currentRefreshRate}
              step={.05}
              marks
              min={.05}
              max={1.0}
              onChange={this.onRefreshRateSelect}
                />
        <Slider
                defaultValue={0}
                aria-labelledby="discrete-slider"
                valueLabelDisplay="auto"
                value={this.state.currentPattern}
                step={1}
                marks
                min={0}
                max={8}
                onChange={this.onPatternSelect}
              />
        <Grid 
            container 
            spacing={2}
            direction="column"
            justify="flex-start"
            alignItems="center"
          >
          <Grid item xs>
            <div>
              {this.state.lightPattern}
            </div>
          </Grid>
          <Grid item xs>
            <Thermometer
                theme="dark"
                value={this.state.temperature}
                max="100"
                steps="3"
                format="°F"
                size="large"
                height="300"
              />
          </Grid>    
        </Grid>
        <Websocket url={`wss:/${window.location.host}/`}
              onMessage={this.handleData}/>
      </div>
    );
  }
}