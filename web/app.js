const express = require('express')
const app = express()
const sensor = require("node-dht-sensor");
const path = require('path')
const fs = require('fs')
const ini = require('ini')
const expressWs = require('express-ws')(app);

const aWss = expressWs.getWss('/')

app.use(express.static(path.join(__dirname, 'build')))

app.ws('/', function(ws, req) {
  ws.on('message', function(msg) {
    aWss.clients.forEach(client => {
      client.send(msg);
    })
  });
});

app.get('/getRefreshRate', async function (req, res) {
  try {
    var config = ini.parse(fs.readFileSync('./sharedConfig/lightcontroller.ini', 'utf-8'))
   
    res.send({refreshRate: config.LightController.RefreshRate})
  } catch (e) {
    console.log(e)
    res.sendStatus(500)
  }
})

app.get('/getPattern', async function (req, res) {
  try {
    var config = ini.parse(fs.readFileSync('./sharedConfig/lightcontroller.ini', 'utf-8'))
   
    res.send({pattern: config.LightController.PatternSelection})
  } catch (e) {
    console.log(e)
    res.sendStatus(500)
  }
})

app.put('/setRefreshRate', async function (req, res) {
  try {
    var config = ini.parse(fs.readFileSync('./sharedConfig/lightcontroller.ini', 'utf-8'))
    config.LightController.RefreshRate = req.query.value
    fs.writeFileSync('./sharedConfig/lightcontroller.ini', ini.stringify(config))

    res.sendStatus(200)
  } catch(e) {
    console.log(e)
    res.sendStatus(500)
  }
})

app.put('/setPattern', async function (req, res) {
  try {
    var config = ini.parse(fs.readFileSync('./sharedConfig/lightcontroller.ini', 'utf-8'))
    config.LightController.PatternSelection = req.query.value
    fs.writeFileSync('./sharedConfig/lightcontroller.ini', ini.stringify(config))

    res.sendStatus(200)
  } catch(e) {
    console.log(e)
    res.sendStatus(500)
  }
})

app.get('/getTemperature', async function(req, res){
  sensor.read(22, 4, function(err, temperature, humidity) {
    if (!err) {
      let tempFahrenheit = (temperature * (9/5)) + 32 
      res.send({temperature: tempFahrenheit, humidity: humidity})
    } else {
      res.sendStatus(500)
    }
  });
})

app.get('/', async (req, res) => {
    try {
        res.sendFile(path.join(__dirname, 'build/app.html'))
    } catch(e) {
        console.log(e)
        res.sendStatus(500)
    }
})

app.listen(3000, () => console.log(`App listening on port 3000!`))